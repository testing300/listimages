package com.peterpartner.testapp.repository

import com.peterpartner.testapp.R
import com.peterpartner.testapp.models.CountryModel
import com.peterpartner.testapp.repository.CountryTranslates.AE_NAME
import com.peterpartner.testapp.repository.CountryTranslates.AE_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.BN_NAME
import com.peterpartner.testapp.repository.CountryTranslates.BN_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.BR_NAME
import com.peterpartner.testapp.repository.CountryTranslates.BR_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.CN_NAME
import com.peterpartner.testapp.repository.CountryTranslates.CN_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.EN_NAME
import com.peterpartner.testapp.repository.CountryTranslates.EN_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.ES_NAME
import com.peterpartner.testapp.repository.CountryTranslates.ES_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.FR_NAME
import com.peterpartner.testapp.repository.CountryTranslates.FR_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.ID_NAME
import com.peterpartner.testapp.repository.CountryTranslates.ID_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.IN_NAME
import com.peterpartner.testapp.repository.CountryTranslates.IN_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.JP_NAME
import com.peterpartner.testapp.repository.CountryTranslates.JP_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.KR_NAME
import com.peterpartner.testapp.repository.CountryTranslates.KR_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.MY_NAME
import com.peterpartner.testapp.repository.CountryTranslates.MY_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.PK_NAME
import com.peterpartner.testapp.repository.CountryTranslates.PK_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.PT_NAME
import com.peterpartner.testapp.repository.CountryTranslates.PT_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.RU_NAME
import com.peterpartner.testapp.repository.CountryTranslates.RU_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.TH_NAME
import com.peterpartner.testapp.repository.CountryTranslates.TH_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.TL_NAME
import com.peterpartner.testapp.repository.CountryTranslates.TL_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.TR_NAME
import com.peterpartner.testapp.repository.CountryTranslates.TR_TWO_LETTER_COUNTRY_CODE
import com.peterpartner.testapp.repository.CountryTranslates.VN_NAME
import com.peterpartner.testapp.repository.CountryTranslates.VN_TWO_LETTER_COUNTRY_CODE

@Suppress("PropertyName")
abstract class AbstractCountriesRepository : CountriesRepository {

    val ENGLISH_LANGUAGE = CountryModel(
        EN_NAME,
        EN_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_usa
    )
    val RUSSIAN_LANGUAGE = CountryModel(
        RU_NAME,
        RU_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_russia
    )
    val INDONESIAN_LANGUAGE = CountryModel(
        ID_NAME,
        ID_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_indonesia
    )
    val MALAYSIAN_LANGUAGE = CountryModel(
        MY_NAME,
        MY_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_malaysia
    )
    val SPANISH_LANGUAGE = CountryModel(
        ES_NAME,
        ES_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_spain
    )
    val THAI_LANGUAGE = CountryModel(
        TH_NAME,
        TH_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_thailand
    )
    val VIETNAM_LANGUAGE = CountryModel(
        VN_NAME,
        VN_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_vietnam
    )
    val CHINESE_LANGUAGE = CountryModel(
        CN_NAME,
        CN_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_china
    )
    val TURKISH_LANGUAGE = CountryModel(
        TR_NAME,
        TR_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_turkey
    )
    val KOREAN_LANGUAGE = CountryModel(
        KR_NAME,
        KR_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_south_korea
    )
    val HINDI_LANGUAGE = CountryModel(
        IN_NAME,
        IN_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_india
    )
    val BRAZILIAN_LANGUAGE = CountryModel(
        BR_NAME,
        BR_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_brazil
    )
    val ARABIC_LANGUAGE = CountryModel(
        AE_NAME,
        AE_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_arabic
    )
    val TAGALOG_LANGUAGE = CountryModel(
        TL_NAME,
        TL_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_phillipines
    )

    val PAKISTAN_LANGUAGE = CountryModel(
        PK_NAME,
        PK_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_pakistan
    )

    val BANGLADESH_LANGUAGE = CountryModel(
        BN_NAME,
        BN_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_bangladesh
    )

    val PORTUGAL_LANGUAGE = CountryModel(
        PT_NAME,
        PT_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_brazil
    )

    val JAPANESE_LANGUAGE = CountryModel(
        JP_NAME,
        JP_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_japan
    )

    val FRENCH_LANGUAGE = CountryModel(
        FR_NAME,
        FR_TWO_LETTER_COUNTRY_CODE,
        R.drawable.ic_lang_icon_franch
    )

}