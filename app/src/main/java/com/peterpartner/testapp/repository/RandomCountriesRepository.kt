package com.peterpartner.testapp.repository

import com.peterpartner.testapp.models.CountryModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlin.random.Random
import kotlin.random.nextInt
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

class RandomCountriesRepository : AbstractCountriesRepository() {

    private val fullList = listOf(
        ENGLISH_LANGUAGE,
        RUSSIAN_LANGUAGE,
        INDONESIAN_LANGUAGE,
        MALAYSIAN_LANGUAGE,
        SPANISH_LANGUAGE,
        THAI_LANGUAGE,
        VIETNAM_LANGUAGE,
        CHINESE_LANGUAGE,
        TURKISH_LANGUAGE,
        KOREAN_LANGUAGE,
        HINDI_LANGUAGE,
        BRAZILIAN_LANGUAGE,
        ARABIC_LANGUAGE,
        TAGALOG_LANGUAGE,
        PAKISTAN_LANGUAGE,
        BANGLADESH_LANGUAGE,
        PORTUGAL_LANGUAGE,
        JAPANESE_LANGUAGE,
        FRENCH_LANGUAGE,
    )

    override val countriesList: Flow<Result<List<CountryModel>>> = flow {
        delay(1.5.seconds)//Request delay
        emit(Result.success(randomisedList()))
        while (true) {
            delay(10.seconds)//Delay before update data from server
            emit(Result.success(randomisedList()))
        }
    }

    override suspend fun invalidateCountriesList(): Result<List<CountryModel>> {
        delay(1.5.seconds)//Request delay
        return Result.success(randomisedList())
    }

    private fun randomisedList(): List<CountryModel> {
        val mutableFullList = fullList.toMutableList()
        repeat(5) {
            val indexToRemove = Random.nextInt(mutableFullList.indices)
            mutableFullList.removeAt(indexToRemove)
        }
        return mutableFullList.toList()
    }
}