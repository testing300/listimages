package com.peterpartner.testapp.view

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.peterpartner.testapp.R
import com.peterpartner.testapp.models.CountryModel
import com.peterpartner.testapp.viewmodel.CountriesViewModel

@Composable
fun CountriesScreen(viewModel: CountriesViewModel) {
    CustomAppTheme {
        Box(modifier = Modifier.fillMaxSize()) {
            Button(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(16.dp)
                    .fillMaxWidth(),
                onClick = {
                    viewModel.moveSelectedToTop(emptyList())//TODO
                },
            ) {
                Text(stringResource(R.string.save_changes_text))
            }
        }
    }
}


@Composable
fun CountryItem(country: CountryModel) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .height(48.dp)
        .clip(MaterialTheme.shapes.small)
        .padding(12.dp)
    ){
        //Image
        //size 24*24
        //padding end 12

        //Text

        //Image
        //size 8*8
    }
}