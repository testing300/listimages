package com.peterpartner.testapp.models

import androidx.annotation.DrawableRes

/**
 * @param countryName Name of language which will be displayed to user. Instance of [String].
 * @param countryTwoLettersLanguageCode Language own two-letters code. Instance of [String].
 * @param countryImageResource Int resource of drawable of current language. Instance of [Int].
 * @author Artyom Tarasov
 */
data class CountryModel(
    val countryName: String,
    val countryTwoLettersLanguageCode: String,
    @DrawableRes val countryImageResource: Int,
)