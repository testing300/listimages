package com.peterpartner.testapp.repository

import com.peterpartner.testapp.models.CountryModel
import kotlinx.coroutines.flow.Flow

interface CountriesRepository {

    /**Return flow contains list with periodical update*/
    val countriesList: Flow<Result<List<CountryModel>>>

    /**Return current state from server*/
    suspend fun invalidateCountriesList(): Result<List<CountryModel>>

}