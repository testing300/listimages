package com.peterpartner.testapp.common

import com.peterpartner.testapp.models.CountryModel

interface CountryFragmentCallback {

    fun onItemClick(locale: CountryModel)

    fun onSaveBtnClick()

}
