package com.peterpartner.testapp.recycler

import com.peterpartner.testapp.common.CountryViewHolderAbstract
import com.peterpartner.testapp.databinding.ItemCountryBinding
import com.peterpartner.testapp.models.CountryModel

class CountryViewHolder(binding: ItemCountryBinding) : CountryViewHolderAbstract(binding.root) {

    override fun bind(countryModel: CountryModel) {
        TODO("Not yet implemented")
    }

    override fun selectOrDeselect(countryModel: CountryModel) {
        TODO("Not yet implemented")
    }
}