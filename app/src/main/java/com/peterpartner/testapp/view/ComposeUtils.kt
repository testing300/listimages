package com.peterpartner.testapp.view

import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color


@Composable
fun CustomAppTheme(
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = Colors(
            primary = colorPrimary,
            primaryVariant = colorAccent,
            secondary = colorSecondAccent,
            secondaryVariant = colorSecondAccent,
            background = colorPrimaryDark,
            surface = colorCardViewBackground,
            error = colorAppNativeError,
            onPrimary = colorPrimaryLight,
            onSecondary = colorPrimaryLight,
            onBackground = colorPrimaryLight,
            onSurface = colorPrimaryLight,
            onError = colorPrimaryLight,
            isLight = false,
        ),
        content = content,
    )
}

val colorPrimaryLight = Color(0xFFffffff)
val colorAccent = Color(0xFFF07000)
val colorPrimary = Color(0xFF2BAB40)
val colorPrimaryDark = Color(0xFF191F2D)
val colorCardViewBackground = Color(0xFF353E51)
val colorSecondAccent = Color(0xFFF07000)
val colorAppNativeError = Color(0xFFDB4831)