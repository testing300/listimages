package com.peterpartner.testapp.common

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.peterpartner.testapp.models.CountryModel

abstract class CountryViewHolderAbstract(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(countryModel: CountryModel)

    abstract fun selectOrDeselect(countryModel: CountryModel)

}