package com.peterpartner.testapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.peterpartner.testapp.common.CountryFragmentCallback
import com.peterpartner.testapp.databinding.FragmentCountryBinding
import com.peterpartner.testapp.models.CountryModel
import com.peterpartner.testapp.viewmodel.CountriesViewModel

class CountriesFragment : Fragment() {

    private val viewModel: CountriesViewModel by viewModels()

    private var _binding: FragmentCountryBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View = if (APP_USED_COMPOSE) {
        ComposeView(requireContext()).also {
            it.setContent {
                CountriesScreen(viewModel)
            }
        }
    } else {
        FragmentCountryBinding.inflate(inflater, container, false).also {
            _binding = it
        }.root
    }

    companion object {
        fun newInstance() = CountriesFragment()
        //////////////////////////////////////////
        const val APP_USED_COMPOSE = true
    }
}