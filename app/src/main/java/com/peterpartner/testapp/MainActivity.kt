package com.peterpartner.testapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.peterpartner.testapp.databinding.MainActivityBinding
import com.peterpartner.testapp.view.CountriesFragment

class MainActivity : AppCompatActivity() {

    lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater, null, false)
        setContentView(binding.root)

        if (savedInstanceState == null) {
            navigateToCountiesList()
        }
    }

    private fun navigateToCountiesList() {
        supportFragmentManager.commit {
            replace(R.id.container, CountriesFragment.newInstance())
        }
    }
}